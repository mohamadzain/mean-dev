FROM node:0.12

MAINTAINER Matthias Luebken, matthias@catalyst-zero.com

# Install gem sass for  grunt-contrib-sass
RUN apt-get update -qq && apt-get install -y build-essential
RUN apt-get install -y ruby
RUN gem install sass

## require to build kerberos dependency for mongodb
RUN apt-get update && apt-get install -y libkrb5-dev

WORKDIR /home/mean

# Install Mean.JS Prerequisites
RUN npm install -g grunt-cli
RUN npm install -g bower
RUN npm install -g gulp
RUN npm install -g node-gyp
RUN npm install -g node-inspector

# this 2 lines below help to solve npm install hang during build
RUN npm update
RUN npm config set registry http://registry.npmjs.org/

# Install Mean.JS packages
ADD package.json /home/mean/package.json
RUN npm install

# Copy kendoui files
COPY kendo-ui/js/* /home/mean/public/lib/kendo-ui/js/
COPY kendo-ui/styles/* /home/mean/public/lib/kendo-ui/styles/


# Manually trigger bower. Why doesnt this work via npm install?
ADD .bowerrc /home/mean/.bowerrc
ADD bower.json /home/mean/bower.json
RUN bower install --config.interactive=false --allow-root

# Make everything available for start
ADD . /home/mean

# Set development environment as default
ENV NODE_ENV development

# Port 3000 for server
# Port 35729 for livereload
EXPOSE 3000 35729
CMD ["gulp"]
